package com.eznamo.screennotes.daos;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.eznamo.screennotes.models.ScanHistoryEntry;

import java.util.List;

/**
 * Created by ezzou on 08/01/2018.
 */
@Dao
public interface ScanEntriesDao {
    @Query("SELECT * FROM scan_entry_history")
    List<ScanHistoryEntry> getAllScanEntries();
    @Insert
    void saveScan(ScanHistoryEntry scanHistoryEntry);
    @Delete
    void deleteScan(ScanHistoryEntry scanHistoryEntry);
}
