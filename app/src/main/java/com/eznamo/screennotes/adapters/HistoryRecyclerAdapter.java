package com.eznamo.screennotes.adapters;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.eznamo.screennotes.R;
import com.eznamo.screennotes.Utils.SNUtils;
import com.eznamo.screennotes.database.DatabaseService;
import com.eznamo.screennotes.models.ScanHistoryEntry;
import java.util.List;

/**
 * Created by ezzou on 09/12/2017.
 */

public class HistoryRecyclerAdapter extends RecyclerView.Adapter {

    List<ScanHistoryEntry> _scanHistoryEntries;
    Context _context;
    LayoutInflater _inflater;

    public HistoryRecyclerAdapter(Context context,List<ScanHistoryEntry> scanHistoryEntryList){
        _scanHistoryEntries = scanHistoryEntryList;
        _context = context;
        _inflater = LayoutInflater.from(_context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View historyEntryView = _inflater.inflate(R.layout.history_cell_layout,parent,false);
        return new HistoryEntryViewHolder(historyEntryView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        HistoryEntryViewHolder historyHolder = (HistoryEntryViewHolder) holder;
        historyHolder._contentTextView.setText(_scanHistoryEntries.get(position).get_scanText());
        historyHolder._dateTextView.setText(SNUtils.getDateFromMillies(_scanHistoryEntries.get(position).get_dateInMilis()));

        historyHolder._copyTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyToClipboard(position);
            }
        });

        historyHolder._shareTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareScan(position);
            }
        });

        historyHolder._deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteScan(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return _scanHistoryEntries.size();
    }

    private class HistoryEntryViewHolder extends RecyclerView.ViewHolder {
        TextView _dateTextView;
        TextView _contentTextView;
        ImageView _copyTextButton;
        ImageView _shareTextButton;
        ImageView _deleteButton;

        public HistoryEntryViewHolder(View historyEntryView) {
            super(historyEntryView);
            _dateTextView = (TextView) historyEntryView.findViewById(R.id.history_cell_date);
            _contentTextView = (TextView) historyEntryView.findViewById(R.id.history_cell_content_text);
            _copyTextButton = (ImageView) historyEntryView.findViewById(R.id.history_cell_copy_icon);
            _shareTextButton = (ImageView) historyEntryView.findViewById(R.id.history_cell_share_icon);
            _deleteButton = (ImageView) historyEntryView.findViewById(R.id.history_cell_delete_icon);
        }
    }

    public void updateScanList(List<ScanHistoryEntry> scanHistoryEntryList)
    {
        _scanHistoryEntries = scanHistoryEntryList;
        notifyDataSetChanged();
    }


    private void deleteScan(final int position) {
        final ScanHistoryEntry scanToDelete = _scanHistoryEntries.get(position);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                DatabaseService.getInstance().getDatabase().scanDao().deleteScan(scanToDelete);
            }
        });
        _scanHistoryEntries.remove(position);
        notifyDataSetChanged();
    }


    private void shareScan(int position) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT,_scanHistoryEntries.get(position).get_scanText());
        Intent chooser = shareIntent.createChooser(shareIntent,_context.getString(R.string.choose_app_to_share_text));
        _context.startActivity(chooser);
    }

    private void copyToClipboard(int position) {
        ClipboardManager clipboardManager = (ClipboardManager) _context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("scan",_scanHistoryEntries.get(position).get_scanText());
        clipboardManager.setPrimaryClip(clipData);
        Toast.makeText(_context,_context.getString(R.string.copied_to_clipboard_sucess),Toast.LENGTH_SHORT).show();
    }
}
