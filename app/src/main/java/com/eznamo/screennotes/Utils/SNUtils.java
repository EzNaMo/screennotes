package com.eznamo.screennotes.Utils;

import com.crashlytics.android.Crashlytics;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ezzou on 10/12/2017.
 */

public class SNUtils {
    public static String getDateFromMillies(long millis)
    {
        try {
            Date date =  new Date(millis);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E dd/MM/yyyy HH:mm:ss");
            return  simpleDateFormat.format(date);
        }
        catch (Exception e)
        {
            Crashlytics.logException(e);
            return "";
        }
    }
}
