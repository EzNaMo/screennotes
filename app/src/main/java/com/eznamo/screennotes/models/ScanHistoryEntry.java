package com.eznamo.screennotes.models;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by ezzou on 10/12/2017.
 */

@Entity(tableName = "scan_entry_history")
public class ScanHistoryEntry {

    @PrimaryKey(autoGenerate = true)
    private int _id;

    @ColumnInfo(name = "scan_date")
    private long _dateInMilis;

    @ColumnInfo(name = "scan_text")
    private String _scanText;


    public ScanHistoryEntry(long _dateInMilis, String _scanText) {
        this._dateInMilis = _dateInMilis;
        this._scanText = _scanText;
    }

    public long get_dateInMilis() {
        return _dateInMilis;
    }

    public void set_dateInMilis(long _dateInMilis) {
        this._dateInMilis = _dateInMilis;
    }

    public String get_scanText() {
        return _scanText;
    }

    public void set_scanText(String _scanText) {
        this._scanText = _scanText;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }
}
