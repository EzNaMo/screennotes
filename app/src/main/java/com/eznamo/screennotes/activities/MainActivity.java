package com.eznamo.screennotes.activities;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.eznamo.screennotes.R;
import com.eznamo.screennotes.database.DatabaseService;
import com.google.android.gms.common.api.BooleanResult;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraUtils;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Hdr;
import com.otaliastudios.cameraview.SessionType;
import com.yalantis.ucrop.UCrop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    static final String TAG = MainActivity.class.getName();
    static final String CROPPED_IMAGE = "CROPPED_IMAGE";
    static final String CROPPED_TO_BE_IMAGE = "CROPPED_TO_BE_IMAGE";
    static final int PERMISSION_ALL = 1;

    CameraView _cameraView;
    ImageButton _captureImageButton,_historyImageButton,_cropImageButton,_scanImageButton, _backImageButton;
    ImageView _capturedImagePreview;
    LinearLayout _bottomMenuLayout;
    Bitmap _currentImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set up database service
        setUpDatabase();
        // Find all view components and set listeners etc
        setUpUi();
        // Set up camera and callbacks
        setUpCameraView();
    }

    private void setUpDatabase() {
        DatabaseService.getInstance().init(getApplication());
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!AskPermissionsIfNeeded());
        _cameraView.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        _cameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        _cameraView.destroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //if the result is coming from the cropping activity handle the image
        if (resultCode == RESULT_OK && requestCode == UCrop.REQUEST_CROP) {
            HandleCropingResult(data);
        } else if (resultCode == UCrop.RESULT_ERROR) {
            HandleCroppingError(data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_ALL: {
                // If request is cancelled, the result arrays are empty.
                for (int i = 0 ; i < grantResults.length ; i ++ ) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        // TODO Display message alerting that the permissions are needed for the application to work properly
                        return;
                    }

                 }
                _cameraView.start();
            }
        }
    }

    /**
     * Get the image result from the cropping activity and displays it
     * @param data this is the data received by the cropping activity
     */
    private void HandleCropingResult(Intent data) {
        // get result image from cropping uri
        final Uri resultUri = UCrop.getOutput(data);
        try
        {
            // get the bitmap image from the uri
            Bitmap resultImage = MediaStore.Images.Media.getBitmap(this.getContentResolver(), resultUri);
            _currentImage = resultImage;
            _capturedImagePreview.setImageBitmap(resultImage);
        }
        catch(IOException ioEx)
        {
            Log.e(TAG,ioEx.getMessage(),ioEx.getCause());
        }
    }

    /**
     * Log the error that occurred during the cropping of the image
     * @param data this is the data received by the cropping activity
     */
    private void HandleCroppingError(Intent data) {
        final Throwable cropError = UCrop.getError(data);
        Log.e(TAG,"Error occurred during image cropping",cropError);
    }

    /**
     * Set up the camera,
     * Set the sessionType to picture,
     * Add callback to be called when picture is taken
     */
    private void setUpCameraView() {
        _cameraView.setSessionType(SessionType.PICTURE);
        _cameraView.setHdr(Hdr.ON);

        //Set listener on cameraView
        _cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] jpeg) {HandlePictureTaken(jpeg);} // handle taken picture
        });
    }

    private void HandlePictureTaken(byte[] jpeg) {
        CameraUtils.decodeBitmap(jpeg, new CameraUtils.BitmapCallback() {
            @Override
            public void onBitmapReady(Bitmap bitmap) {
                showPreviewImageMode(bitmap);
            }
        });
    }

    private void OnCroppingClick(View view)
    {
        String destinationFileName = CROPPED_IMAGE;
        UCrop uCrop = UCrop.of(getImageUri(this,_currentImage),Uri.fromFile(new File(getCacheDir(), destinationFileName)));
        uCrop.start(MainActivity.this);
    }


    /**
     * activate the preview mode where a captured image is displayed
     * in this mode options are visible for modifying the captured image
     * this previewed image can be scanned by the text detector
     * @param bitmap
     */
    private void showPreviewImageMode(Bitmap bitmap) {
        // stop the camera
        _cameraView.stop();
        _cameraView.setVisibility(View.GONE);
        // hide unwanted views for preview mode
        _captureImageButton.setVisibility(View.GONE);
         // show wanted views for preview mode
        _bottomMenuLayout.setVisibility(View.VISIBLE);
        _backImageButton.setVisibility(View.VISIBLE);
        _capturedImagePreview.setVisibility(View.VISIBLE);

        //apply the bitmap to the imageview to preview it
        _currentImage = bitmap;
        _capturedImagePreview.setImageBitmap(_currentImage);
    }

    /**
     * Create a text recognizer and scan the image for text
     */
    private void performImageScanning() {
        TextRecognizer textRecognizer = new TextRecognizer.Builder(this)
                .build();
        // if text reconizer not available send a message
        if (!textRecognizer.isOperational()) {
            new AlertDialog.Builder(this)
                    .setMessage("Please wait until the system finish initializing").show();
            return;
        }
        // create frame with our image and run the scanner on it
        Frame frame = new Frame.Builder().setBitmap(_currentImage).build();
        SparseArray<TextBlock> textBlocks = textRecognizer.detect(frame);
        // Order textBlocks from the first to appear on the image to the last : from the top of the image to the bottom
        ArrayList<TextBlock> sortedFromTopToBottom = (ArrayList<TextBlock>) asList(textBlocks);
        Collections.sort(sortedFromTopToBottom, new Comparator<TextBlock>() {
            @Override
            public int compare(TextBlock t1, TextBlock t2) {
                if (t1.getBoundingBox().top > t2.getBoundingBox().top)
                    return 1;
                if (t1.getBoundingBox().top < t2.getBoundingBox().top)
                    return -1;
                return 0;
                }
            });

        String detectedText = "";
        for (int i = 0; i < sortedFromTopToBottom.size(); i++) {
            TextBlock textBlock = sortedFromTopToBottom.get(i);
            if (textBlock != null && textBlock.getValue() != null) {
                detectedText += textBlock.getValue() + '\n';
            }
        }
        // start the activity for text result display
        Intent resultDisplayIntent = new Intent(this,TextResultActivity.class);
        resultDisplayIntent.putExtra(TextResultActivity.TEXT_RESULT_DATA,detectedText);
        startActivity(resultDisplayIntent);

        Log.d(TAG,detectedText);
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, CROPPED_TO_BE_IMAGE, null);
        return Uri.parse(path);
    }

    private void setUpUi() {
        setContentView(R.layout.activity_main);
        _captureImageButton = (ImageButton) findViewById(R.id.capture_image_button);
        _historyImageButton = (ImageButton) findViewById(R.id.history_button);
        _scanImageButton = (ImageButton) findViewById(R.id.scan_text_button);
        _cameraView = (CameraView) findViewById(R.id.camera_preview_surfaceview);
        _bottomMenuLayout = (LinearLayout) findViewById(R.id.bottom_image_option_menu);
        _capturedImagePreview = (ImageView) findViewById(R.id.captured_image_preview);
        _cropImageButton = (ImageButton) findViewById(R.id.crop_image_button);
        _backImageButton = (ImageButton) findViewById(R.id.back_button);
        //Set on click listeners
        _captureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(_cameraView != null && _cameraView.isStarted())
                    _cameraView.capturePicture();
            }
        });

        _backImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // restart the camera
                _cameraView.start();
                _cameraView.setVisibility(View.VISIBLE);
                _currentImage = null;

                // show wanted views for normal mode
                _captureImageButton.setVisibility(View.VISIBLE);
                // hide unwanted views for normal mode
                _bottomMenuLayout.setVisibility(View.GONE);
                _backImageButton.setVisibility(View.GONE);
                _capturedImagePreview.setVisibility(View.GONE);
            }
        });

        _historyImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent historyActivity = new Intent(MainActivity.this,HistoryActivity.class);
                startActivity(historyActivity);
            }
        });

        _scanImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performImageScanning();
            }
        });

        _cropImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                OnCroppingClick(view);
            }
        });

    }

    public boolean AskPermissionsIfNeeded()
    {
        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
        boolean isPermissionNeeded = !hasPermissions(this, PERMISSIONS);
        if(isPermissionNeeded){
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        return isPermissionNeeded;
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static <C> List<C> asList(SparseArray<C> sparseArray) {
        if (sparseArray == null) return null;
        List<C> arrayList = new ArrayList<C>(sparseArray.size());
        for (int i = 0; i < sparseArray.size(); i++)
            arrayList.add(sparseArray.valueAt(i));
        return arrayList;
    }




}
