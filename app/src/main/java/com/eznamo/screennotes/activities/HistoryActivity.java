package com.eznamo.screennotes.activities;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.eznamo.screennotes.R;
import com.eznamo.screennotes.adapters.HistoryRecyclerAdapter;
import com.eznamo.screennotes.database.DatabaseService;
import com.eznamo.screennotes.models.ScanHistoryEntry;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    RecyclerView _historyRecyclerView;
    HistoryRecyclerAdapter _historyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_layout);
        _historyRecyclerView = (RecyclerView) findViewById(R.id.history_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayout.VERTICAL,false);
        _historyRecyclerView.setLayoutManager(linearLayoutManager);
        _historyAdapter = new HistoryRecyclerAdapter(this,Collections.<ScanHistoryEntry>emptyList());
        _historyRecyclerView.setAdapter(_historyAdapter);
        loadScanHistoryList();
    }

    public void loadScanHistoryList()
    {
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                final List<ScanHistoryEntry> scanHistoryEntries = DatabaseService.getInstance().getDatabase().scanDao().getAllScanEntries();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        _historyAdapter.updateScanList(scanHistoryEntries);
                    }
                });
            }
        });
    }
}
