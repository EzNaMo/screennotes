package com.eznamo.screennotes.activities;
import android.content.Intent;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.eznamo.screennotes.R;
import com.eznamo.screennotes.database.DatabaseService;
import com.eznamo.screennotes.models.ScanHistoryEntry;

import java.util.Date;

/**
 * Created by ezzou on 28/10/2017.
 */

public class TextResultActivity extends AppCompatActivity {

    static final String TEXT_RESULT_DATA = "TEXT_RESULT_DATA";


    TextView _resultTextView;
    ImageButton _saveButton;
    String _resultText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.text_result_display_layout);
        _resultTextView = (TextView) findViewById(R.id.result_text_view);
        _saveButton = (ImageButton) findViewById(R.id.save_text_button);

        Intent data = getIntent();
        String detectedText = data.getStringExtra(TEXT_RESULT_DATA);
        _resultText = detectedText;

        if(_resultText != null && !_resultText.isEmpty())
        {
            _resultTextView.setText(_resultText);

        }
        else
        {
            _resultTextView.setText(getString(R.string.no_result_text_detected));
        }

        _saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveScanToDatabase();
            }
        });

    }

    private void saveScanToDatabase() {
        if(_resultText != null && !_resultText.isEmpty())
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                ScanHistoryEntry scanHistoryEntry = new ScanHistoryEntry(System.currentTimeMillis(),_resultText);
                DatabaseService.getInstance().getDatabase().scanDao().saveScan(scanHistoryEntry);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(TextResultActivity.this,getString(R.string.scan_saved_success),Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }

}
