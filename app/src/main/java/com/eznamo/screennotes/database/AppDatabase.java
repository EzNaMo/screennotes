package com.eznamo.screennotes.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.eznamo.screennotes.daos.ScanEntriesDao;
import com.eznamo.screennotes.models.ScanHistoryEntry;

/**
 * Created by ezzou on 08/01/2018.
 */
@Database(entities = {ScanHistoryEntry.class} ,version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ScanEntriesDao scanDao();
}
