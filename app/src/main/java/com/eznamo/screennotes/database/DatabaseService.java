package com.eznamo.screennotes.database;

import android.arch.persistence.room.Room;
import android.content.Context;

/**
 * Created by ezzou on 08/01/2018.
 */

public class DatabaseService {

    private static final DatabaseService ourInstance = new DatabaseService();

    static public DatabaseService getInstance() {
        return ourInstance;
    }

    private AppDatabase _database;
    private Context _applicationContext;

    private DatabaseService() {
    }

    public void init(Context applicationContext) {
        _applicationContext = applicationContext;
    }

    public AppDatabase getDatabase(){
        if(_database == null)
            _database = Room.databaseBuilder(_applicationContext,AppDatabase.class,"screen_note_database").build();
        return  _database;
    }
}
